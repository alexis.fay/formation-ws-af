package fr.formation.alexis.fay.mediatheque1cxf.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import fr.formation.alexis.fay.mediatheque1cxf.serveur.ILivreService;

public class ClientMain {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		//Creation d'un objet Usine à Bean Proxy, voir cours
		System.out.println("Client livres : ");
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();	
		//on lie a cet objet, l'adresse du serveur et l'interface du service du serveur
		factory.setAddress("http://localhost:9001/livres");
		factory.setServiceClass(ILivreService.class);
		//en plus d'ajouter l'adresse et la classe, ajout d'objets de logger en entrée et en sortie
		factory.getInInterceptors().add(new LoggingInInterceptor());
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		//on crée un objet qui utilise une interface et on l'utilise.
		ILivreService livresServices = factory.create(ILivreService.class);
		System.out.println(livresServices.getInfos());
		System.out.println("Livre 4 empruntable : "+livresServices.estEmpruntable(4));
		try {
		System.out.println("Livre -3 empruntabe : "+livresServices.estEmpruntable(-3));
		} catch(Exception ex) {
			System.err.println("Exception : "+ex.getMessage());
		}
		System.out.println("Date retour livre 1 : "+livresServices.getRetour(1));
		System.out.println("Livre du mois : "+livresServices.getLivreDuMois());
		System.out.println("livre du mois de Mars : "+livresServices.getLivresDeLAnnee()[2].getTitre());
	}
}
