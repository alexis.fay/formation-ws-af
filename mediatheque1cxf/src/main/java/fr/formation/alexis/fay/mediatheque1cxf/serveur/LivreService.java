package fr.formation.alexis.fay.mediatheque1cxf.serveur;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.jws.WebService;

@WebService
public class LivreService implements ILivreService{
	public String getInfos() {
		return "l'entrée de la bibliotheque se trouve Avenue VictorHugo";
	}
	
	public boolean estEmpruntable(int id) {
		if(id<1) {
			throw new IllegalArgumentException("id doit être 1 ou plus");
		} else { 
			return false; 
		}
	}
	
	public Date getRetour(int id) {
		LocalDate d = LocalDate.now().plusDays(10);
		return Date.from(d.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}
	
	public Livre getLivreDuMois() {
		Livre l = new Livre("La plus secrete...", "M. M. Sarr", 2021);
		//DataSource dataSource = new FileDataSource("image.jfif");
		//DataHandler dataHandler = new DataHandler(dataSource);
		//l.setImage(dataHandler);
		return l;
	}
	
	public Livre[] getLivresDeLAnnee() {
		Livre[] livres = new Livre[12];
		for (int i =0; i<12; i++) {
			livres[i] = getLivreDuMois();
		}
		return livres;
	}


}

