package fr.formation.alexis.fay.mediatheque2spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CdController {

	private CdService cdService;

	public CdService getCdService() {
		return cdService;
	}

	@Autowired
	public void setCdService(CdService cdService) {
		this.cdService = cdService;
	}
	
	@RequestMapping (path="/cd/nombre", produces=MediaType.APPLICATION_XML_VALUE)
	public int getNombre() {
		return cdService.getNombreCd();
	}
	
	@RequestMapping(path="/cd", method=RequestMethod.POST)
	public void ajoute(@RequestBody Cd cd) {
			cdService.ajouteCd(cd);
	}
	
	@RequestMapping(path="/cd", method=RequestMethod.GET)
	public List<Cd> getTous(){
		return cdService.getCds();
	}
	
	//num est composé de 1 ou plusieur chiffres : \\d+
	@RequestMapping(path="/cd/{num:\\d+}", method=RequestMethod.GET)
	public Cd getUn(@PathVariable("num") int n) {
		return cdService.getCd(n);
	}
	
	@RequestMapping (path="/cd/{num:\\d+}/titre", method=RequestMethod.PUT)
	public void changeTitre(@PathVariable("num") int n, @RequestBody String titre) {
		cdService.changeTitre(n, titre);
	}
	
	@RequestMapping(path="/json/cd", method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Cd> getTousJson() {
		return cdService.getCds();
	}
}
