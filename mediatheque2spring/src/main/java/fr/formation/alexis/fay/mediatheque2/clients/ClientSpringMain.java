package fr.formation.alexis.fay.mediatheque2.clients;




import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.formation.alexis.fay.mediatheque2spring.Cd;

public class ClientSpringMain {
	
	public static void main(String[] args) {
		try {
			System.out.println("Client Java Spring");
			//Objet Rest Template de Spring pour lire de façon Spring sur un service Rest
			RestTemplate rt = new RestTemplate();
			//sur cet objet je le connecte à une URL et je precise ce que je lis
			String health = rt.getForObject("http://localhost:9002/health", String.class);
			System.out.println("Health : "+health);
			Integer nbCd = rt.getForObject("http://localhost:9002/cd/nombre", Integer.class);
			System.out.println("Nombre cd : "+nbCd);
			Cd cd1 = new Cd("Abbey Road", "The Beatles", 1966);
			Cd cd2 = new Cd("Feu", "Nekfeu", 2016);
			rt.postForObject("http://localhost:9002/cd", cd1, Void.class);
			rt.postForObject("http://localhost:9002/cd", cd2, Void.class);
			/*Solution 1 :
			 * ParameterizedTypeReference<List<Cd>> ref = 
					new ParameterizedTypeReference<List<Cd>>() {};
			ResponseEntity<List<Cd>> cdsEntity = 
					rt.exchange("http://localhost:9002/cd", HttpMethod.GET, null, ref);
			List<Cd> cds = cdsEntity.getBody();
			System.out.println("Tous les Cds :");
			for (Cd cd:cds) {
				System.out.println(cd);
			} */
			
			//solution2:
			Cd[] cds=rt.getForObject("http://localhost:9002/cd", Cd[].class);
			System.out.println("Tous les Cds :");
			for (Cd cd:cds) {
				System.out.println(cd);
			} 
			Cd cd0=rt.getForObject("http://localhost:9002/cd/0", Cd.class);
			System.out.println("Premier CD : "+cd0);
			
			
			rt.put("http://localhost:9002/cd/0/titre", "Help!", String.class);
		
		} catch(Exception e) {
			System.err.println(e);
		}
	}
}