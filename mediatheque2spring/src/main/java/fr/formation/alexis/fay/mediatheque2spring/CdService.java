package fr.formation.alexis.fay.mediatheque2spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class CdService {
	//je dis juste que cette variable doit avoir les fonctionnalité de l'interface List
	private List<Cd> cds;
	
	public CdService() {
		//ArrayList est une implémentation de l'interface List
		//C'est une veritable classe avec du code donc c'est autorisé
		//de déclarer une List et de la transformé en ArrayList
		cds = new ArrayList<Cd>();
	}
	
	public int getNombreCd() {
        return cds.size();
    }
	
	public void ajouteCd(Cd cd) {
		cds.add(cd);
	}
	
	public List<Cd> getCds(){
		return cds;
	}
	
	public Cd getCd (int n) {
		return cds.get(n);
	}
	
	public void changeTitre (int n, String titre) {
		cds.get(n).setTitre(titre);
	}
}
