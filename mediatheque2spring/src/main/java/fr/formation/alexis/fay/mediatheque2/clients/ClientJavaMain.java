package fr.formation.alexis.fay.mediatheque2.clients;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class ClientJavaMain {

	public static void main(String[] args) {
		try {
			System.out.println("Client Java");
			//déclaration d'URL
			URL url = new URL("http://localhost:9002/health");
			//Obtention d'une URLConnextion
			URLConnection conn=url.openConnection();
			//pour cette adresse Health = juste la lecture donc un Reader (qu'on transforme a partir d'un InputStream)
			InputStream is = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String reponse = br.readLine();
			System.out.println("health : "+reponse);
			br.close();
			
		} catch(Exception e) {
			System.err.println(e);
		}

	}

}
