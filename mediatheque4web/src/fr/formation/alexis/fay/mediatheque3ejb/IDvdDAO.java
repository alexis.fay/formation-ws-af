package fr.formation.alexis.fay.mediatheque3ejb;

import java.util.List;

import javax.ejb.Remote;

@Remote
public interface IDvdDAO {
	public int getNombre();
	public void ajoute(Dvd dvd);
	public Dvd lire(int id);
	public List<Dvd> lireTous();
}
