package fr.formation.alexis.fay.mediatheque4web;

import java.io.IOException;
import java.io.Writer;
import java.time.LocalTime;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.formation.alexis.fay.mediatheque3ejb.Dvd;
import fr.formation.alexis.fay.mediatheque3ejb.IDvdDAO;
import fr.formation.alexis.fay.mediatheque3ejb.IDvdtheque;

@WebServlet("/dvds")
public class DvdsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public DvdsServlet() {
    }
	
    private IDvdtheque dvdtheque ;
    private IDvdDAO dvdDAO;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		Writer out = response.getWriter();
		out.write("<!DOCTYPE html><html><body>");
		out.write("<h1>DVDthèque</h1>");
		out.write("<p>"+dvdtheque.getInfos()+"</p>");
		out.write("<p>Ouvert à 9h ? "+dvdtheque.ouvertA(LocalTime.of(9, 0))+"</p>");
		out.write("<p>Quand ? "+dvdtheque.getDerniereInterrogation()+"</p>");
		
		out.write("<table><thead><tr><th>id</th><th>Titre</th><th>Année</th></td></thead><tbody>");
		for(Dvd dvd:dvdDAO.lireTous()) {
			out.write("<tr><td>"+dvd.getId()+"</td><td>"+dvd.getTitre()+"</td><td>"+dvd.getAnnee()+"</td></tr>");
		}
		out.write("</tbody></table>");
		out.write("</body></html>");
		out.close();
	}
	
	@EJB(lookup="mediatheque3ejb/DVDs!fr.formation.alexis.fay.mediatheque3ejb.IDvdtheque")
	public void setDvdtheque (IDvdtheque dvdtheque) {
		this.dvdtheque = dvdtheque;
	}
	
	@EJB(lookup="ejb:/mediatheque3ejb/DvdDAO!fr.formation.alexis.fay.mediatheque3ejb.IDvdDAO")
	public void setDvdDAO(IDvdDAO dvdDAO) {
		this.dvdDAO = dvdDAO;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
