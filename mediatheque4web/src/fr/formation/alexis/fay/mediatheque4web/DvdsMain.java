package fr.formation.alexis.fay.mediatheque4web;

import java.time.LocalTime;

import javax.naming.Context;
import javax.naming.InitialContext;

import fr.formation.alexis.fay.mediatheque3ejb.Dvd;
import fr.formation.alexis.fay.mediatheque3ejb.IDvdDAO;
import fr.formation.alexis.fay.mediatheque3ejb.IDvdtheque;

public class DvdsMain {

	public static void main(String[] args) {
		System.out.println("Dvd : client lourd");
		try {
		//objet context (flux à ouvrir)
		Context context = new InitialContext();
		IDvdtheque dvdtheque = (IDvdtheque) context.lookup(
				"mediatheque3ejb/DVDs!fr.formation.alexis.fay.mediatheque3ejb.IDvdtheque");
		System.out.println("informations : "+dvdtheque.getInfos());
		System.out.println("Ouvert à 17h30 ? "+dvdtheque.ouvertA(LocalTime.of(17, 30)));
		IDvdDAO dvdDAO = (IDvdDAO) context.lookup("ejb:/mediatheque3ejb/DvdDAO!fr.formation.alexis.fay.mediatheque3ejb.IDvdDAO");
		Dvd dvd1 = new Dvd("Taxi", 1998);
		Dvd dvd2 = new Dvd("Inception", 2010);
		dvdDAO.ajoute(dvd1);
		dvdDAO.ajoute(dvd2);
		System.out.println(dvdDAO.lire(156));
		System.out.println(dvdDAO.lireTous());
		System.out.println("Après l'ajout, j'ai "+dvdDAO.getNombre()+" DVD");
		context.close();
		} catch(Exception e) {
			System.err.println(e);
		}
	}

}
