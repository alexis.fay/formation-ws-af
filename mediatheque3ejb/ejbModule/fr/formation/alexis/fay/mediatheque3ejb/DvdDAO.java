package fr.formation.alexis.fay.mediatheque3ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(name="DvdDAO")
public class DvdDAO implements IDvdDAO {
	
	private EntityManager em;
	
	public int getNombre() {
		return em.createQuery("select count(*) from Dvd", Long.class).getSingleResult().intValue();
	}

	public void ajoute(Dvd dvd) {
		em.persist(dvd);
	}

	public Dvd lire(int id) {
		return em.find(Dvd.class, id);
	}

	public List<Dvd> lireTous() {
		return em.createQuery("from Dvd order by annee", Dvd.class).getResultList();
	}

	public EntityManager getEm() {
		return em;
	}

	@PersistenceContext(unitName="DvdPU")
	public void setEm(EntityManager em) {
		this.em = em;
	}
	
}
